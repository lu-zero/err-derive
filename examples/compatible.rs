#![allow(deprecated)]

// Example compatible with Rust 1.15

#[macro_use]
extern crate err_derive;

use std::error::Error;
use std::path::PathBuf;

#[derive(Debug, Error)]
pub enum FormatError {
    #[error(display = "invalid header (expected: {:?}, got: {:?})", expected, found)]
    InvalidHeader {
        expected: String,
        found: String,
    },
    #[error(display = "missing attribute: {:?}", _0)]
    MissingAttribute(String),

}

#[derive(Debug, Error)]
pub enum LoadingError {
    #[error(display = "could not decode file")]
    FormatError(#[error(cause)] FormatError),
    #[error(display = "could not find file: {:?}", path)]
    NotFound { path: PathBuf },
}

impl From<FormatError> for LoadingError {
    fn from(f: FormatError) -> Self {
        LoadingError::FormatError(f)
    }
}

fn main() {
    let my_error: LoadingError = FormatError::MissingAttribute("some_attr".to_owned()).into();

    print_error(&my_error);
}

fn print_error(e: &dyn Error) {
    eprintln!("error: {}", e);
    let mut cause = e.cause();
    while let Some(e) = cause {
        eprintln!("caused by: {}", e);
        cause = e.cause();
    }
}
